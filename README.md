TestApp
==============

Шаблон для простого приложения Vaadin, для работы которого требуется только контейнер Servlet 3.0.

Рабочий процесс
==================

Чтобы скомпилировать весь проект, запустите «mvn install».

Чтобы запустить приложение, запустите «mvn jetty: run» и откройте http: // localhost: 8080 /.

Чтобы создать развертываемую WAR в производственном режиме:
- измените productionMode на true в конфигурации servlet класса (вложенный в класс UI)
- запустите "mvn clean package"
- протестируйте WAR файл с помощью "mvn jetty: run-war"

Компиляция на стороне клиента
----------------------------------

Сгенерированный проект maven по умолчанию использует автоматически созданный набор виджетов. Когда вы добавляете 
зависимость, которая требует компиляции на стороне клиента, плагин maven автоматически сгенерирует ее для вас. 
Ваши собственные настройки на стороне клиента могут быть добавлены в пакет «client».

Отладка кода на стороне клиента
   - запустите "mvn vaadin: run-codeserver" на отдельной консоли во время работы приложения
   - активируйте Super Dev Mode в окне отладки приложения

Разработка темы с использованием компилятора времени выполнения
------------------------------------------------------------------

При разработке темы Vaadin можно настроить для компиляции темы на основе SASS во время выполнения на сервере.
Таким образом, вы можете просто изменить файлы scss в своей среде IDE и перезагрузить браузер, чтобы увидеть изменения.

Чтобы использовать компиляцию среды выполнения, откройте pom.xml и закомментируйте цель compile-theme из конфигурации
vaadin-maven-plugin. Чтобы удалить, возможно, существующую предварительно скомпилированную тему, запустите 
«mvn clean package» один раз.

При использовании компилятора среды выполнения запуск приложения в режиме «выполнения» (а не в режиме «отладки») \
может значительно ускорить последовательные компиляции тем.

Настоятельно рекомендуется отключить компиляцию среды выполнения для производственных файлов WAR.

Использование предварительных релизов Vaadin
------------------------------------------------

Если предварительные выпуски Vaadin не включены по умолчанию, используйте параметр Maven «-P vaadin-prerelease» 
или измените значение активации по умолчанию для профиля в pom.xml.

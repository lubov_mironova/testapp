package domain;

import java.util.List;

public class City {
    /**
     * Название города
     */
    private String name;
    /**
     * Идентификатор города
     */
    private Integer id;
    /**
     * Список улиц в городе
     */
    private List<Street> streets;

    public City(String name, Integer id, List<Street> streets) {
        this.name = name;
        this.id = id;
        this.streets = streets;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<Street> getStreets() {
        return streets;
    }

    public void setStreets(List<Street> streets) {
        this.streets = streets;
    }
}

package org.example;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import domain.City;
import domain.Street;

import javax.servlet.annotation.WebServlet;
import java.util.ArrayList;
import java.util.List;

/**
 * This UI is the application entry point. A UI may either represent a browser window
 * (or tab) or some part of a html page where a Vaadin application is embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is intended to be
 * overridden to add component to the user interface and initialize non-component functionality.
 */
@Theme("mytheme")
public class MyUI extends UI {

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        final VerticalLayout layout = new VerticalLayout();
        List<City> cities = fillsListWithStreets();

        final ComboBox<City> cityCB = new ComboBox<>("Выберите город");
        cityCB.setItems(cities);
        cityCB.setItemCaptionGenerator(City::getName);

        final ComboBox<Street> streetCB = new ComboBox<>("Выберите улицу");
        streetCB.setEnabled(false);
        cityCB.addValueChangeListener(event -> {
            City selected = event.getValue();
            streetCB.setEnabled(true);
            if (selected != null) {
                streetCB.setItems(selected.getStreets());
            }
        });
        streetCB.setItemCaptionGenerator(Street::getName);

        streetCB.addValueChangeListener(valueChangeEvent -> {
            Street choice = valueChangeEvent.getValue();
            if (choice != null) {
                displayNotification(choice.getId());
            }
        });

        layout.addComponents(cityCB, streetCB);
        setContent(layout);
    }

    /**
     * Возвращает заполненный список городов с улицами
     *
     * @return список городов
     */
    private List<City> fillsListWithStreets() {
        List<Street> moscowStreets = new ArrayList<>();
        moscowStreets.add(new Street("Маршала Жукова", 24));
        moscowStreets.add(new Street("Водников", 72));
        moscowStreets.add(new Street("Кольская", 41));
        moscowStreets.add(new Street("Свободы", 92));
        moscowStreets.add(new Street("Чечерина", 38));

        List<Street> penzaStreets = new ArrayList<>();
        penzaStreets.add(new Street("Московская", 13));
        penzaStreets.add(new Street("Ладожская", 28));
        penzaStreets.add(new Street("Кирова", 19));
        penzaStreets.add(new Street("Радужная", 45));
        penzaStreets.add(new Street("Проспект Победы", 59));

        List<Street> samaraStreets = new ArrayList<>();
        samaraStreets.add(new Street("Ближняя", 17));
        samaraStreets.add(new Street("Илецкая", 25));
        samaraStreets.add(new Street("Манежная", 30));
        samaraStreets.add(new Street("Пестеля", 46));
        samaraStreets.add(new Street("Казачья", 52));

        List<City> cities = new ArrayList<>();
        cities.add(new City("Москва", 5, moscowStreets));
        cities.add(new City("Пенза", 9, penzaStreets));
        cities.add(new City("Самара", 15, samaraStreets));
        return cities;
    }

    /**
     * Отображает уведомление о выбранной улице с идентификатором улицы
     *
     * @param id идентификатор улицы
     */
    private void displayNotification(Integer id) {
        Notification.show("Улица выбрана", String.valueOf(id), Notification.Type.HUMANIZED_MESSAGE);
    }

    @WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = MyUI.class, productionMode = false)
    public static class MyUIServlet extends VaadinServlet {
    }
}
